# springSecurity

#### 介绍
 springSecurity前后端分离+全局异常捕获+token挤掉线+token续期+动态资源授权+验证码+前端明文加密

#### 软件架构
SpringSecurity+MybatisPuls+JWT+Redis+Mysql

使用方法
1.resource下找到sql文件导入数据库中
2.修改yml配置文件数据库地址
3.启动redis

接口文档:https://www.apifox.cn/apidoc/shared-7d6e542d-f032-4024-a041-edb6a5edadd6/api-32473373