package com.yujie.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yujie.model.SysUser;
import com.yujie.model.dto.SysUserDTO;

import java.util.List;

public interface SysUserDao extends BaseMapper<SysUser> {
    public SysUserDTO getSysUserAndRoleByUserName(String username);
}
