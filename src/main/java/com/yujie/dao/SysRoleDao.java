package com.yujie.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yujie.model.SysRole;
import com.yujie.model.dto.SysRoleDTO;

import javax.management.relation.Role;
import java.util.List;

public interface SysRoleDao extends BaseMapper<SysRole> {
    List<SysRoleDTO> listSysRoleBySysUserId(Integer id);
    List<Role> listSysRoleByUrl(String url);
}
