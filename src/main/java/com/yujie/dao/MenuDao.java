package com.yujie.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yujie.model.Menu;

import java.util.List;

public interface MenuDao extends BaseMapper<Menu> {
    List<Menu> listMenuByRoleId(Integer role);
}
