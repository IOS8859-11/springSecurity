package com.yujie.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 角色和菜单权限
 */
@TableName(value = "sys_role")
public class SysRole implements Serializable {
    private static final long serialVersionUID = 5097008445497386586L;
    @TableId
    private Integer id;
    private String name; //名称

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
