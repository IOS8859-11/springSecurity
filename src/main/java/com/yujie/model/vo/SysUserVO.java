package com.yujie.model.vo;

import com.yujie.model.dto.SysRoleDTO;

import java.util.ArrayList;
import java.util.List;

public class SysUserVO {
    private Integer id;
    private String username; //账号
    private String name;  //姓名
    private String address;  //地址
    private List<SysRoleDTO> roles=new ArrayList<>(); //角色
    private String token;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public List<SysRoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRoleDTO> roles) {
        this.roles = roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "SysUserVO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", roles=" + roles +
                ", token='" + token + '\'' +
                '}';
    }
}
