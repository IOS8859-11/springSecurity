package com.yujie.model.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yujie.model.Menu;

import java.io.Serializable;
import java.util.List;

/**
 * 角色和菜单权限
 */
public class SysRoleDTO implements Serializable {
    private static final long serialVersionUID = 5097008445497386586L;
    @TableId
    private Integer id;
    private String name; //名称
    private List<Menu> menuList;

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", menuList=" + menuList +
                '}';
    }
}
