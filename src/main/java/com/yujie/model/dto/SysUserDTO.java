package com.yujie.model.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yujie.model.SysRole;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SysUserDTO implements Serializable {

    private static final long serialVersionUID = -3410848817498578361L;
    private Integer id;
    private String username; //账号
    private String password; //密码
    private String name;  //姓名
    private String address;  //地址
    private List<SysRoleDTO> roles=new ArrayList<>(); //角色


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public List<SysRoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRoleDTO> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", roles=" + roles +
                '}';
    }
}
