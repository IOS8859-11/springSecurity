package com.yujie.model.query;


import javax.validation.constraints.NotEmpty;

public class UserQuery {

    @NotEmpty(message = "账号不能为空！",groups ={UserQueryCheck.class,UserQueryCheck2.class})
    private String username; //账号
    @NotEmpty(message = "密码不能为空",groups ={UserQueryCheck.class,UserQueryCheck2.class})
    private String password; //密码
    @NotEmpty(message = "姓名不能为空",groups =UserQueryCheck.class)
    private String name;  //姓名
    private String address;  //地址
    @NotEmpty(message = "验证码不能为空",groups ={UserQueryCheck.class,UserQueryCheck2.class})
    private String code; //验证码

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public interface UserQueryCheck {

    }
    public interface UserQueryCheck2 {

    }
}
