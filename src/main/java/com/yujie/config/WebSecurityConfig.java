package com.yujie.config;

import com.yujie.dao.MenuDao;
import com.yujie.filter.AccessDeniedFilter;
import com.yujie.filter.ControllerExceptionFilter;
import com.yujie.filter.JwtAuthenticationTokenFilter;
import com.yujie.filter.XssFilter;
import com.yujie.security.AuthenticationEntryPointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    //采用框架提供的加密方式处理密码
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Autowired
    private MenuDao menuDao;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests().antMatchers("/user/login").anonymous();
        http.authorizeRequests().antMatchers("/user/register").anonymous();
        http.authorizeRequests().antMatchers("/vc").anonymous();
        http.authorizeRequests().anyRequest().authenticated();
        //前后端分离,关闭session功能
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //xss防御
        http.addFilterBefore(new XssFilter(), UsernamePasswordAuthenticationFilter.class);
        //认证过滤
        http.addFilterBefore(new JwtAuthenticationTokenFilter(redisTemplate), UsernamePasswordAuthenticationFilter.class);
        //授权过滤
        http.addFilterAfter(new AccessDeniedFilter(menuDao), FilterSecurityInterceptor.class);
        //Controller异常全局捕获
        http.addFilterAfter(new ControllerExceptionFilter(), FilterSecurityInterceptor.class);

        //认证失败处理
        http.exceptionHandling().authenticationEntryPoint(new AuthenticationEntryPointImpl());
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    //放行静态资源,如果遇到404会被安全框架拦截
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(loadExcludePath());
    }

    private String[] loadExcludePath() {
        return new String[]{
                "/static/**",
                "/templates/**",
                "/img/**",
                "/js/**",
                "/css/**",
                "/fonts/**"};
    }


}
