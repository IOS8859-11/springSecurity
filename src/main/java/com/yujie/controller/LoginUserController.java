package com.yujie.controller;

import com.yujie.common.CommonStatus;
import com.yujie.model.SysUser;
import com.yujie.model.query.UserQuery;
import com.yujie.model.vo.SysUserVO;
import com.yujie.service.SysUserService;
import com.yujie.utils.RSAUtils;
import com.yujie.utils.VerifyCodeUtil;
import com.yujie.utils.WebExceptionUtil;
import com.yujie.utils.ResultJson;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.GroupSequence;
import java.util.Objects;

@RequestMapping("/user")
@RestController
public class LoginUserController {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/register")
    public ResultJson registerUser(@RequestBody @Validated({UserQuery.UserQueryCheck.class}) UserQuery u, BindingResult bindingResult,HttpSession session)  {
        if(bindingResult.hasErrors()){
            String message = bindingResult.getFieldError().getDefaultMessage();
            WebExceptionUtil.controllerException(message);
        }

        //解密
        u.setName(RSAUtils.decode(u.getName()));
        u.setUsername(RSAUtils.decode(u.getUsername()));
        u.setPassword(RSAUtils.decode(u.getPassword()));
        u.setCode(RSAUtils.decode(u.getCode()));

        checkVerifyCode(u.getCode(),session);

        u.setPassword(passwordEncoder.encode(u.getPassword()));
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(u,sysUser);
        try {
            sysUserService.saveSysUser(sysUser);
        } catch (Exception e) {
            WebExceptionUtil.controllerException("注册用户失败",e);
        }

        return new ResultJson(200, CommonStatus.SUCCESS.value());
    }

    @RequestMapping("/login")
    @ResponseBody
    public ResultJson loginUser(@RequestBody @Validated(UserQuery.UserQueryCheck2.class) UserQuery u, BindingResult bindingResult, HttpSession session) {
        if(bindingResult.hasErrors()){
            String message = bindingResult.getFieldError().getDefaultMessage();
            WebExceptionUtil.controllerException(message);
        }
        //解密
        u.setUsername(RSAUtils.decode(u.getUsername()));
        u.setPassword(RSAUtils.decode(u.getPassword()));
        u.setCode(RSAUtils.decode(u.getCode()));
        checkVerifyCode(u.getCode(),session);

        SysUserVO sysUserVO = null;
        try {
            sysUserVO = sysUserService.getLoginUser(u.getUsername(), u.getPassword());
        } catch (Exception e) {
            WebExceptionUtil.controllerException("用户名或密码错误",e);
        }
        return new ResultJson(200,CommonStatus.SUCCESS.value(),sysUserVO);
    }

    @RequestMapping("/index")
    public ResultJson index(){
        return  new ResultJson(200,"首页成功");
    }

    private void checkVerifyCode( String code, HttpSession session){
        String cv = (String) session.getAttribute("code");
        if(!code.equalsIgnoreCase(cv)){
            WebExceptionUtil.controllerException("验证码错误");
        }else{
            //销毁当前的验证码
            session.removeAttribute("code");
        }
    }

}
