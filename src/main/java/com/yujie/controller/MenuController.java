package com.yujie.controller;

import com.yujie.utils.ResultJson;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
public class MenuController {
    @RequestMapping("/menu")
    public ResultJson menuManage(){
        return new ResultJson(200,"success","菜单管理访问成功");
    }
    @RequestMapping("/user")
    public ResultJson userManage(){
        return new ResultJson(200,"success","用户管理访问成功");
    }
    @RequestMapping("/manage")
    public ResultJson systemManage(){
        return new ResultJson(200,"success","系统设置问成功");
    }

}
