package com.yujie.controller;

import com.yujie.utils.ResultJson;
import com.yujie.utils.VerifyCodeUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class CommonController {
    @RequestMapping("/a")
    public ResultJson a(){
        return new ResultJson(200,"success","无需权限a访问成功");
    }

    @RequestMapping("/vc")
    public void createVerifyCode(HttpServletRequest request, HttpServletResponse response){
        VerifyCodeUtil verifyCodeUtil = new VerifyCodeUtil();
        verifyCodeUtil.createVerifyCode(request,response);
    }
}
