package com.yujie.controller;

import com.yujie.utils.ResultJson;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/call")
@RestController
public class CallController {
    @RequestMapping("/center")
    public ResultJson menuManage(){
        return new ResultJson(200,"success","呼叫中心访问成功");
    }
}
