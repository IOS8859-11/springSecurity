package com.yujie.utils;

public class ExecutionTimeUtil {
    public ExecutionTimeUtil(TIMEUNIT timeUNIT){
       this.timeUnit=timeUNIT;
    }

    public ExecutionTimeUtil() {
        this.timeUnit= TIMEUNIT.MILLISECOND;
    }

    public enum TIMEUNIT{
        SECOND,
        MILLISECOND
    }
    private TIMEUNIT timeUnit;

    private long start;
    public void start(){
        start=System.currentTimeMillis();
    }

    public long end(){
        long end=0;
        if(timeUnit== TIMEUNIT.MILLISECOND){
            end= System.currentTimeMillis()-start;
        }else{
            end= (System.currentTimeMillis()-start)/1000;
        }
        return end;
    }



}
