package com.yujie.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 获取简单异常信息
 */
public class StackTraceInfo {
    List<Throwable> arrayLists =new ArrayList<>();
    List<String> info =new ArrayList<>();

    public String stackTraceInfo(Exception e){
        cause(e);
        for (int i = arrayLists.size()-1 ;i >=0 ; i--) {
            Throwable throwable = arrayLists.get(i);
            StackTraceElement[] stackTrace = throwable.getStackTrace();
            for (StackTraceElement s : stackTrace) {
                if(s.getClassName().contains("com.yujie")){
                    info.add(s.toString());
                }
            }
            //找到栈中底部最近的错误
            if(info.size()>0){
                break;
            }
        }
        Collections.reverse(info);
        String strInfo="";
        for (int i = 0; i <info.size() ; i++) {
            if(i!=info.size()-1){
                strInfo+=info.get(i)+"&#10;";
            }else{
                strInfo+=info.get(i);
            }
        }

        return "&#10;"+strInfo;
    }


    private int count=0;
    private Throwable cause(Throwable throwable){
        if(throwable != null){
            arrayLists.add(throwable);
            count++;
            this.cause(throwable.getCause());
        }
        return throwable;
    }
}
