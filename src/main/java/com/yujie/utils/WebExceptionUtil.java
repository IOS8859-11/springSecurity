package com.yujie.utils;

import com.yujie.common.CommonStatus;

import java.io.IOException;

/**
 * 将错误信息打印前端页面
 */
public class WebExceptionUtil {

    /**
     * security认证异常处理
     * @param exceptionMsg 错误信息
     * @param commonStatus 键的名字
     */
    public static void authenticateException(String exceptionMsg, CommonStatus commonStatus) {
        ThreadLocalUtil.set(commonStatus.value(),exceptionMsg);
        throw new RuntimeException(exceptionMsg);
    }
    /**
     * 处理Controller异常
     * @param exceptionMsg 错误信息
     * @param e 异常信息
     */
    public static void controllerException(String exceptionMsg,Exception e) {
        throw new RuntimeException(exceptionMsg,e);
    }

    /**
     * 处理Controller异常
     * @param exceptionMsg 错误信息
     */
    public static void controllerException(String exceptionMsg) {
        throw new RuntimeException(exceptionMsg);
    }


}
