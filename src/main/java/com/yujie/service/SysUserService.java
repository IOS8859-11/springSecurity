package com.yujie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yujie.dao.SysUserDao;
import com.yujie.model.SysUser;
import com.yujie.model.dto.SysUserDTO;
import com.yujie.model.vo.SysUserVO;

public interface SysUserService {
    SysUserDTO getSysUserAndRoleAndMenuByUserName(String username);
    SysUserVO getLoginUser(String username, String password) throws Exception;
    void saveSysUser(SysUser sysUser) throws Exception;
}
