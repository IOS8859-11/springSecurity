package com.yujie.common;


public enum CommonStatus {
    EXCEPTION("exception"),
    STACKTRACE("stackTrace"),
    SUCCESS("success");

    CommonStatus(String msg) {
        this.value=msg;
    }

    private final String value;

    public String value() {
        return this.value;
    }
}
