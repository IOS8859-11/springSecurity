package com.yujie.filter;

import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;


public class XssFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        XssRequestWrapper xssRequestWrapper =new XssRequestWrapper(httpServletRequest);
        filterChain.doFilter(xssRequestWrapper,httpServletResponse);

    }
}

class XssRequestWrapper extends HttpServletRequestWrapper {

     XssRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    /**
     *
     * @param name
     * @return
     */
    @Override
    public String getHeader(String name) {
        String value = super.getHeader(name);
        if(Objects.nonNull(value)){
            value =  HtmlUtils.htmlEscape(value);
        }
        return value;
    }

    @Override
    public String getParameter(String name) {
        //获取页面数据
        String value = super.getParameter(name);
        //进行字符转义
        if(Objects.nonNull(value)){
            value = HtmlUtils.htmlEscape(value);
            System.out.println("转义后的字符：" + value);
        }
        return  value;
    }

    @Override
    public String[] getParameterValues(String name) {
        String[] values = super.getParameterValues(name);
        if(values != null){
            String[] escValues = new String[values.length];
            for (int i=0; i<values.length; i++) {
                if(Objects.nonNull(values[i])){
                    escValues[i] = HtmlUtils.htmlEscape(values[i]);
                }
            }
            return escValues;
        }
        return super.getParameterValues(name);
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        InputStream in= super.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int len = -1;
        byte[] buffer = new byte[1024];//1kb
        while ((len = in.read(buffer)) != -1) {
            baos.write(buffer, 0, len);
        }
        in.close();
        String json = new String(baos.toByteArray());
        json=json.replaceAll("<","&lt;").replaceAll(">","&gt;");
        InputStream bain = new ByteArrayInputStream(json.getBytes());
        //匿名内部类，只需要重写read方法，把转义后的值，创建成ServletInputStream对象
        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return bain.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }
        };
    }

}