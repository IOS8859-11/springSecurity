package com.yujie.filter;


import com.alibaba.fastjson.JSON;
import com.yujie.common.CommonStatus;
import com.yujie.model.dto.SysRoleDTO;
import com.yujie.model.dto.SysUserDTO;
import com.yujie.utils.*;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * 处理Controller抛出异常会被该过滤器处理
 */
public class ControllerExceptionFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            filterChain.doFilter(servletRequest, response);
        } catch (Exception e) {
            e.printStackTrace();
            String causeStr="";
            Throwable var1 = e.getCause();
            if(var1 != null){
                //获取更简洁的错误信息
                Throwable cause = var1.getCause();
                if(cause !=null){
                    causeStr=cause.toString();
                    causeStr=causeStr.replaceAll("\r\n","&#10;");
                }
            }
            String stackTraceInfo = new StackTraceInfo().stackTraceInfo(e);
              ResultJson resultJson = new ResultJson(500,var1==null?"":var1.getMessage(),stackTraceInfo);
              WebUtils.renderString((HttpServletResponse) response, JSON.toJSONString(resultJson));
        }
    }


}
