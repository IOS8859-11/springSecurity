package com.yujie.security;


import com.yujie.common.CommonStatus;
import com.yujie.model.dto.SysUserDTO;
import com.yujie.service.SysUserService;
import com.yujie.utils.WebExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;
@Service
public class SysUserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private SysUserService sysUserService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUserDTO sysUserDTO = sysUserService.getSysUserAndRoleAndMenuByUserName(username);
        if(Objects.isNull(sysUserDTO)){
            WebExceptionUtil.controllerException("用户名不存在");
        }
        return new LoginUser(sysUserDTO);
    }


}
