package com.yujie.security;


import com.alibaba.fastjson.annotation.JSONField;
import com.yujie.model.dto.SysUserDTO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class LoginUser implements UserDetails {
    private SysUserDTO sysUser;
    private String newToken;

    public LoginUser() {
    }

    public LoginUser(SysUserDTO sysUser) {
        this.sysUser = sysUser;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return null;
    }

    public String getNewToken() {
        return newToken;
    }

    public void setNewToken(String newToken) {
        this.newToken = newToken;
    }

    @Override
    public String getPassword() {
        return sysUser.getPassword();
    }

    @Override
    public String getUsername() {
        return sysUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public SysUserDTO getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUserDTO sysUser) {
        this.sysUser = sysUser;
    }



}
