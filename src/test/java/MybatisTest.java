import com.SpringBootApplicationDemo;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yujie.dao.MenuDao;
import com.yujie.dao.SysUserDao;
import com.yujie.model.Menu;
import com.yujie.model.SysUser;
import com.yujie.model.dto.SysUserDTO;
import com.yujie.service.SysUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServlet;
import java.util.List;
import java.util.regex.Pattern;

@SpringBootTest(classes = SpringBootApplicationDemo.class)
public class MybatisTest extends HttpServlet {
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private MenuDao menuDao;

    @Autowired
    private SysUserService sysUserService;
    @Test
    public void selectOne(){
        QueryWrapper<SysUser> userQueryWrapper = new QueryWrapper<>();
        SysUser sysUser = new SysUser();
        sysUser.setUsername("xjr");
        userQueryWrapper.eq("username",sysUser.getUsername());
        SysUser sysUser1 = sysUserDao.selectOne(userQueryWrapper);
        System.out.println(sysUser1);
    }

    @Test
    public void selectByUerNameSysUserAndRole(){
        SysUserDTO xjr = sysUserService.getSysUserAndRoleAndMenuByUserName("xajr");
        String json = JSON.toJSONString(xjr);
        System.out.println(json);
    }

    @Test
    public void a(){
        String reg="/system/menu/.*";
        String str="/system/menu/";
        boolean f=Pattern.matches(reg,str);
        System.out.println(f);

    }


    @Test
    public void a2(){
        List<Menu> menuList = menuDao.selectList(null);
        IPage<Menu> menuIPage = menuDao.selectPage(null, null);
        System.out.println(menuIPage.getSize());
    }



}
